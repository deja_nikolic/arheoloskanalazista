﻿namespace ArheoloskaNalazistaSrbija
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.tbxZavodIzmena = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxOpstinaIzmena = new System.Windows.Forms.TextBox();
            this.tbxMestoIzmena = new System.Windows.Forms.TextBox();
            this.tbxGradskaIzmena = new System.Windows.Forms.TextBox();
            this.tbxAdresaIzmena = new System.Windows.Forms.TextBox();
            this.tbxNazivIzmena = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Nadlezni zavod:";
            // 
            // tbxZavodIzmena
            // 
            this.tbxZavodIzmena.Location = new System.Drawing.Point(123, 216);
            this.tbxZavodIzmena.Name = "tbxZavodIzmena";
            this.tbxZavodIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxZavodIzmena.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Opstina:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Mesto:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Gradska opstina:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Adresa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Naziv:";
            // 
            // tbxOpstinaIzmena
            // 
            this.tbxOpstinaIzmena.Location = new System.Drawing.Point(123, 190);
            this.tbxOpstinaIzmena.Name = "tbxOpstinaIzmena";
            this.tbxOpstinaIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxOpstinaIzmena.TabIndex = 16;
            // 
            // tbxMestoIzmena
            // 
            this.tbxMestoIzmena.Location = new System.Drawing.Point(123, 164);
            this.tbxMestoIzmena.Name = "tbxMestoIzmena";
            this.tbxMestoIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxMestoIzmena.TabIndex = 15;
            // 
            // tbxGradskaIzmena
            // 
            this.tbxGradskaIzmena.Location = new System.Drawing.Point(123, 138);
            this.tbxGradskaIzmena.Name = "tbxGradskaIzmena";
            this.tbxGradskaIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxGradskaIzmena.TabIndex = 14;
            // 
            // tbxAdresaIzmena
            // 
            this.tbxAdresaIzmena.Location = new System.Drawing.Point(123, 112);
            this.tbxAdresaIzmena.Name = "tbxAdresaIzmena";
            this.tbxAdresaIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxAdresaIzmena.TabIndex = 13;
            // 
            // tbxNazivIzmena
            // 
            this.tbxNazivIzmena.Location = new System.Drawing.Point(123, 86);
            this.tbxNazivIzmena.Name = "tbxNazivIzmena";
            this.tbxNazivIzmena.Size = new System.Drawing.Size(201, 20);
            this.tbxNazivIzmena.TabIndex = 12;
            this.tbxNazivIzmena.TextChanged += new System.EventHandler(this.tbxNazivIzmena_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Azuriraj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Location = new System.Drawing.Point(116, 23);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(35, 13);
            this.lblNaziv.TabIndex = 25;
            this.lblNaziv.Text = "label7";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Menjate nalaziste: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Molimo unesite azurirane podatke.";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 301);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxZavodIzmena);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxOpstinaIzmena);
            this.Controls.Add(this.tbxMestoIzmena);
            this.Controls.Add(this.tbxGradskaIzmena);
            this.Controls.Add(this.tbxAdresaIzmena);
            this.Controls.Add(this.tbxNazivIzmena);
            this.Name = "Form3";
            this.Text = "Arheologija Srbije";
            this.Load += new System.EventHandler(this.Form3_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tbxNazivIzmena;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox tbxZavodIzmena;
        public System.Windows.Forms.TextBox tbxOpstinaIzmena;
        public System.Windows.Forms.TextBox tbxMestoIzmena;
        public System.Windows.Forms.TextBox tbxGradskaIzmena;
        public System.Windows.Forms.TextBox tbxAdresaIzmena;
        public System.Windows.Forms.Button button1;
    }
}