﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace ArheoloskaNalazistaSrbija
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private bool buttonPrikaziClicked = false;
        private bool buttonPrikaziRClicked = false;

        private void btnDodajNalaziste_Click(object sender, EventArgs e)
        {
            if (tbxNaziv.Text == "" || tbxMesto.Text == "" || tbxOpstina.Text == "" || tbxZavod.Text == "" || tbxAdresa.Text == "" || tbxGradska.Text == "")
            {
                MessageBox.Show("Morate uneti sve podatke.");
            }
            else
            {
                var conn = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(conn);
                var database = server.GetDatabase("ArheologijaSrbije");

                var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

                Nalazista n1 = new Nalazista { naziv = tbxNaziv.Text, adresa = tbxAdresa.Text, gradskaOpstina =tbxGradska.Text, mesto = tbxMesto.Text, opstina = tbxOpstina.Text, zavod = tbxZavod.Text};

                collection.Insert(n1);

                MessageBox.Show("Uspesno ste dodali nalaziste: " + n1.naziv + " u bazu podataka");

                tbxNaziv.Clear();
                tbxAdresa.Clear();
                tbxMesto.Clear();
                tbxZavod.Clear();
                tbxOpstina.Clear();
                tbxGradska.Clear();
                cbxDodajNalazista.Items.Add(n1.naziv).ToString();

            }
        }

      

        private void button1_Click(object sender, EventArgs e) //Azuriranje
        {
            if (listBox1.Items.Count > 0)
            {
                if (listBox1.SelectedIndex > -1)
                {
                    string curItem = listBox1.SelectedItem.ToString();
                    int index = listBox1.FindString(curItem);
                    if (index != -1)
                    {
                        Form3 fm3 = new Form3(this);
                        fm3.ShowDialog();

                        var conn = "mongodb://localhost/?safe=true";
                        var server = MongoServer.Create(conn);
                        var database = server.GetDatabase("ArheologijaSrbije");

                        var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

                        var query = Query.EQ("naziv", curItem);
                        var update = MongoDB.Driver.Builders.Update.Set("naziv", fm3.tbxNazivIzmena.Text);
                        var update1 = MongoDB.Driver.Builders.Update.Set("adresa", fm3.tbxAdresaIzmena.Text);
                        var update2 = MongoDB.Driver.Builders.Update.Set("gradskaOpstina", fm3.tbxGradskaIzmena.Text);
                        var update3 = MongoDB.Driver.Builders.Update.Set("mesto", fm3.tbxMestoIzmena.Text);
                        var update4 = MongoDB.Driver.Builders.Update.Set("opstina", fm3.tbxOpstinaIzmena.Text);
                        var update5 = MongoDB.Driver.Builders.Update.Set("zavod", fm3.tbxZavodIzmena.Text);

                        if (fm3.btnAzuriraj == true)
                        {
                            collection.Update(query, update);
                            collection.Update(query, update1);
                            collection.Update(query, update2);
                            collection.Update(query, update3);
                            collection.Update(query, update4);
                            collection.Update(query, update5);
                           
                        }
                    }

                    
                    
                }
                else
                    {
                    MessageBox.Show("Morate izabrati nalaziste");
                }
            }
           
        }

    


        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            int buttonPrikaziCounter = 0;
            buttonPrikaziClicked = true;
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

            MongoCursor<Nalazista> nalazista = collection.FindAll();

            if (buttonPrikaziClicked)
            {
                foreach (Nalazista n in nalazista.ToArray<Nalazista>())
                {
                   
                    listBox1.Items.Add(n.naziv).ToString();
                    listAdr.Items.Add(n.adresa).ToString();
                    listOps.Items.Add(n.opstina).ToString();
                    listGrad.Items.Add(n.gradskaOpstina).ToString();
                    listMesto.Items.Add(n.mesto).ToString();
                    listZavod.Items.Add(n.zavod).ToString();
                 
                    
                }
                buttonPrikaziCounter++;
            }
            if (buttonPrikaziCounter > 0) { 
               
                listBox1.Items.Clear();
                listAdr.Items.Clear();
                listOps.Items.Clear();
                listGrad.Items.Clear();
                listMesto.Items.Clear();
                listZavod.Items.Clear();
                cbxDodajNalazista.Items.Clear();
                foreach (Nalazista n in nalazista.ToArray<Nalazista>())
                {
                    
                    listBox1.Items.Add(n.naziv).ToString();
                    listAdr.Items.Add(n.adresa).ToString();
                    listOps.Items.Add(n.opstina).ToString();
                    listGrad.Items.Add(n.gradskaOpstina).ToString();
                    listMesto.Items.Add(n.mesto).ToString();
                    listZavod.Items.Add(n.zavod).ToString();
                    cbxDodajNalazista.Items.Add(n.naziv).ToString();

                }


            }


            
        }

        public string Transfer
        {
            get { return listBox1.SelectedItem.ToString(); }
        }
       



        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
           
            int counter = 0;
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

            MongoCursor<Nalazista> nalazista = collection.FindAll();

            foreach (Nalazista n in collection.FindAll())
            { if (n.naziv == tbxIzbrisi.Text)
                    counter++;
            }

           
           if (counter>0)
           {
                    var query = Query.EQ("naziv", tbxIzbrisi.Text);

                    collection.Remove(query);

                    MessageBox.Show("Uspesno ste izbrisali nalaziste: " + tbxIzbrisi.Text + " iz baze.");

                    cbxDodajNalazista.Items.Remove(tbxIzbrisi.Text);
            }
           else
            {
                    MessageBox.Show("Nalaziste: " + tbxIzbrisi.Text + " ne postoji u bazi.");
            }
            



        }

        private void button2_Click(object sender, EventArgs e)
        {
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var nalazista = database.GetCollection<Nalazista>("ArheoloskaNalazista");
            nalazista.RemoveAll();

            
            MessageBox.Show("Uspesno ste izbrisali sva nalazista iz baze.");
            cbxDodajNalazista.Items.Clear();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            int counter = 0;
            if (tbxImeA.Text == ""  || tbxJMBG.Text == "")
            {
              
                    MessageBox.Show("Morate uneti ime i jedinstveni maticni broj.");
                
            }
            else
            {
                var conn = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(conn);
                var database = server.GetDatabase("ArheologijaSrbije");

                var collection = database.GetCollection<Arheolozi>("Arheolozi");

                foreach(Arheolozi a in collection.FindAll())
                {
                    if (a.JMBG == tbxJMBG.Text)
                        counter++;
                }

                if (counter == 0)
                {
                    Arheolozi a1 = new Arheolozi { ime = tbxImeA.Text, JMBG = tbxJMBG.Text, firma = tbxFirmaA.Text, otkrica = new List<string> { tbxIskopineA.Text } };

                    var nal = database.GetCollection<Nalazista>("ArheoloskaNalazista");
                    collection.Insert(a1);


                    MessageBox.Show("Uspesno ste dodali arheologa: " + a1.ime + " u bazu podataka");

                    
                    if (cbxDodajNalazista.Items.Count > 0 && cbxDodajNalazista.SelectedIndex > -1)
                    {
                        string curItem = cbxDodajNalazista.SelectedItem.ToString();
                        var query = Query.EQ("naziv", curItem);


                        foreach (Nalazista n in nal.Find(query))
                        {




                            int index = cbxDodajNalazista.FindString(curItem);

                            if (index != -1)
                            {


                               
                                a1.Nalazistaa.Add(new MongoDBRef("ArheoloskaNalazista", curItem));
                                n.Arheolozi = new MongoDBRef("Arheolozi", a1.Id);
                                nal.Save(n);
                                
                            }





                        }

                        collection.Save(a1);

                    }
                }
                else
                {
                    MessageBox.Show("U bazi vec postoji arheolog sa ovim maticnim brojem.");
                }
                

            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
         
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

            MongoCursor<Nalazista> nalazista = collection.FindAll();

            
                foreach (Nalazista n in nalazista.ToArray<Nalazista>())
                {
                    
                    cbxDodajNalazista.Items.Add(n.naziv).ToString();

                }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int buttonPrikaziCounter = 0;
            buttonPrikaziRClicked = true;
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Arheolozi>("Arheolozi");
            var nal = database.GetCollection<Nalazista>("ArheoloskaNalazista");



            if (buttonPrikaziRClicked) {
                foreach (Arheolozi n in collection.FindAll())
                {
                    foreach (MongoDBRef nalRef in n.Nalazistaa.ToList())
                    {
                        Nalazista nalazista = database.FetchDBRefAs<Nalazista>(nalRef);
                        lbIme.Items.Add(n.ime).ToString();
                        lbFirma.Items.Add(n.firma).ToString();
                        lbNal.Items.Add(nalRef).ToString();
                        //lbNal.Items.Add(nalazista.naziv).ToString();
                    }

                }
                buttonPrikaziCounter++;
            }

            if (buttonPrikaziCounter > 0)
            {
                lbIme.Items.Clear();
                lbFirma.Items.Clear();
                lbNal.Items.Clear();

                foreach (Arheolozi n in collection.FindAll())
                {
                    foreach (MongoDBRef nalRef in n.Nalazistaa.ToList())
                    {
                        Nalazista nalazista = database.FetchDBRefAs<Nalazista>(nalRef);
                        lbIme.Items.Add(n.ime).ToString();
                        lbFirma.Items.Add(n.firma).ToString();
                        lbNal.Items.Add(nalRef).ToString();

                    }

                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");
            var arh = database.GetCollection<Arheolozi>("Arheolozi");
          
            arh.RemoveAll();

            MessageBox.Show("Uspesno ste izbrsali sve arheologe iz baze.");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int counter = 0;
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Arheolozi>("Arheolozi");

            MongoCursor<Arheolozi> arheolozi = collection.FindAll();

            if (tbxJMBGIzbrisi.Text == "" || tbxIzbrisiA.Text == "")
            {
                MessageBox.Show("Morate popuniti oba polja");
            }
            else
            {

                foreach (Arheolozi n in collection.FindAll())
                {
                    if (n.ime == tbxIzbrisiA.Text && n.JMBG == tbxJMBGIzbrisi.Text)
                        counter++;
                }


                if (counter > 0)
                {
                    var query = Query.And(Query.EQ("ime", tbxIzbrisiA.Text), Query.EQ("JMBG", tbxJMBGIzbrisi.Text));

                    collection.Remove(query);

                    MessageBox.Show("Uspesno ste izbrisali arheologa: " + tbxIzbrisiA.Text + " iz baze.");


                }
                else
                {
                    MessageBox.Show("Arheolog: " + tbxIzbrisiA.Text + " ne postoji u bazi.");
                }
            }

        }

        private void tbxJMBG_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        
    }
}
