﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;

namespace ArheoloskaNalazistaSrbija
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPovezi_Click(object sender, EventArgs e)
        {
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");
            var arhcollection = database.GetCollection<Arheolozi>("Arheolozi");

            
            

            Form2 fm2 = new Form2();
            fm2.ShowDialog();

        }
    }
}
