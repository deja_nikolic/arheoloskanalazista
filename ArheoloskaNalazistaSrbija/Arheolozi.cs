﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ArheoloskaNalazistaSrbija
{
    class Arheolozi
    {
        public ObjectId Id { get; set; }
        public string ime { get; set; }
        public string firma { get; set; }

        public string JMBG { get; set; }
        public List<string> otkrica { get; set; }
        public List<MongoDBRef> Nalazistaa { get; set; }

        public Arheolozi()
        {
            Nalazistaa = new List<MongoDBRef>();
        }
    }
}
