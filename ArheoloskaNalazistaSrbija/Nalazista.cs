﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace ArheoloskaNalazistaSrbija
{
    class Nalazista
    {
        public ObjectId Id { get; set; }

        public string naziv { get; set; }

        public string adresa { get; set; }

        public string gradskaOpstina { get; set; }

        public string mesto { get; set; }

        public string opstina { get; set; }

        public string zavod { get; set; }

        public MongoDBRef Arheolozi { get; set; }
    }
}
