﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace ArheoloskaNalazistaSrbija
{
    public partial class Form3 : Form
    {

        public bool btnAzuriraj = false;
        public Form3(Form2 f2)
        {
            this.f2 = f2;
            InitializeComponent();
        }

        Form2 f2;
        

        private void tbxNazivIzmena_TextChanged(object sender, EventArgs e)
        {
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

            MongoCursor<Nalazista> nalazista = collection.FindAll();

            


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var conn = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(conn);
            var database = server.GetDatabase("ArheologijaSrbije");

            var collection = database.GetCollection<Nalazista>("ArheoloskaNalazista");

            MongoCursor<Nalazista> nalazista = collection.FindAll();
            if (tbxNazivIzmena.Text == "" || tbxAdresaIzmena.Text == "" || tbxGradskaIzmena.Text == "" || tbxMestoIzmena.Text == "" || tbxOpstinaIzmena.Text == "" || tbxZavodIzmena.Text == "")
            {
                MessageBox.Show("Morate uneti sve podatke");
            }
            else
            {
                MessageBox.Show("Uspesno ste azurirali nalaziste");
                btnAzuriraj = true;
            }
        }

        private void Form3_Load_1(object sender, EventArgs e)
        {
            lblNaziv.Text = this.f2.Transfer;
            tbxNazivIzmena.Text = this.f2.Transfer;
           
        }
    }
}
