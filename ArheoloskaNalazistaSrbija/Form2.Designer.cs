﻿namespace ArheoloskaNalazistaSrbija
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajNalaziste = new System.Windows.Forms.Button();
            this.tbxNaziv = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxZavod = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxOpstina = new System.Windows.Forms.TextBox();
            this.tbxMesto = new System.Windows.Forms.TextBox();
            this.tbxGradska = new System.Windows.Forms.TextBox();
            this.tbxAdresa = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.listZavod = new System.Windows.Forms.ListBox();
            this.listOps = new System.Windows.Forms.ListBox();
            this.listMesto = new System.Windows.Forms.ListBox();
            this.listGrad = new System.Windows.Forms.ListBox();
            this.listAdr = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.btnIzbrisi = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxIzbrisi = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cbxDodajNalazista = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.tbxIskopineA = new System.Windows.Forms.TextBox();
            this.tbxFirmaA = new System.Windows.Forms.TextBox();
            this.tbxImeA = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.lbNal = new System.Windows.Forms.ListBox();
            this.lbFirma = new System.Windows.Forms.ListBox();
            this.lbIme = new System.Windows.Forms.ListBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.tbxIzbrisiA = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.tbxJMBG = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbxJMBGIzbrisi = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDodajNalaziste
            // 
            this.btnDodajNalaziste.Location = new System.Drawing.Point(131, 358);
            this.btnDodajNalaziste.Name = "btnDodajNalaziste";
            this.btnDodajNalaziste.Size = new System.Drawing.Size(201, 23);
            this.btnDodajNalaziste.TabIndex = 0;
            this.btnDodajNalaziste.Text = "Dodaj nalaziste";
            this.btnDodajNalaziste.UseVisualStyleBackColor = true;
            this.btnDodajNalaziste.Click += new System.EventHandler(this.btnDodajNalaziste_Click);
            // 
            // tbxNaziv
            // 
            this.tbxNaziv.Location = new System.Drawing.Point(131, 31);
            this.tbxNaziv.Name = "tbxNaziv";
            this.tbxNaziv.Size = new System.Drawing.Size(201, 20);
            this.tbxNaziv.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(631, 425);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.tbxZavod);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.tbxOpstina);
            this.tabPage1.Controls.Add(this.tbxMesto);
            this.tabPage1.Controls.Add(this.tbxGradska);
            this.tabPage1.Controls.Add(this.tbxAdresa);
            this.tabPage1.Controls.Add(this.tbxNaziv);
            this.tabPage1.Controls.Add(this.btnDodajNalaziste);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(623, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dodaj nalaziste";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Nadlezni zavod:";
            // 
            // tbxZavod
            // 
            this.tbxZavod.Location = new System.Drawing.Point(131, 287);
            this.tbxZavod.Name = "tbxZavod";
            this.tbxZavod.Size = new System.Drawing.Size(201, 20);
            this.tbxZavod.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Opstina:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mesto:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Gradska opstina:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Adresa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Naziv:";
            // 
            // tbxOpstina
            // 
            this.tbxOpstina.Location = new System.Drawing.Point(131, 227);
            this.tbxOpstina.Name = "tbxOpstina";
            this.tbxOpstina.Size = new System.Drawing.Size(201, 20);
            this.tbxOpstina.TabIndex = 5;
            // 
            // tbxMesto
            // 
            this.tbxMesto.Location = new System.Drawing.Point(131, 174);
            this.tbxMesto.Name = "tbxMesto";
            this.tbxMesto.Size = new System.Drawing.Size(201, 20);
            this.tbxMesto.TabIndex = 4;
            // 
            // tbxGradska
            // 
            this.tbxGradska.Location = new System.Drawing.Point(131, 126);
            this.tbxGradska.Name = "tbxGradska";
            this.tbxGradska.Size = new System.Drawing.Size(201, 20);
            this.tbxGradska.TabIndex = 3;
            // 
            // tbxAdresa
            // 
            this.tbxAdresa.Location = new System.Drawing.Point(131, 74);
            this.tbxAdresa.Name = "tbxAdresa";
            this.tbxAdresa.Size = new System.Drawing.Size(201, 20);
            this.tbxAdresa.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.listZavod);
            this.tabPage2.Controls.Add(this.listOps);
            this.tabPage2.Controls.Add(this.listMesto);
            this.tabPage2.Controls.Add(this.listGrad);
            this.tabPage2.Controls.Add(this.listAdr);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.btnPrikazi);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.listBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(623, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Azuriraj nalaziste";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(498, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 15);
            this.label13.TabIndex = 15;
            this.label13.Text = "Zavod";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(305, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 15);
            this.label12.TabIndex = 14;
            this.label12.Text = "Mesto";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(400, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 15);
            this.label11.TabIndex = 13;
            this.label11.Text = "Opstina";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(204, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 15);
            this.label10.TabIndex = 12;
            this.label10.Text = "Gradska opstina";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(106, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 15);
            this.label9.TabIndex = 11;
            this.label9.Text = "Adresa";
            // 
            // listZavod
            // 
            this.listZavod.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listZavod.FormattingEnabled = true;
            this.listZavod.HorizontalScrollbar = true;
            this.listZavod.Location = new System.Drawing.Point(501, 24);
            this.listZavod.Name = "listZavod";
            this.listZavod.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listZavod.Size = new System.Drawing.Size(92, 329);
            this.listZavod.TabIndex = 10;
            // 
            // listOps
            // 
            this.listOps.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listOps.FormattingEnabled = true;
            this.listOps.HorizontalScrollbar = true;
            this.listOps.Location = new System.Drawing.Point(403, 24);
            this.listOps.Name = "listOps";
            this.listOps.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listOps.Size = new System.Drawing.Size(92, 329);
            this.listOps.TabIndex = 9;
            // 
            // listMesto
            // 
            this.listMesto.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listMesto.FormattingEnabled = true;
            this.listMesto.HorizontalScrollbar = true;
            this.listMesto.Location = new System.Drawing.Point(305, 24);
            this.listMesto.Name = "listMesto";
            this.listMesto.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listMesto.Size = new System.Drawing.Size(92, 329);
            this.listMesto.TabIndex = 8;
            // 
            // listGrad
            // 
            this.listGrad.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listGrad.FormattingEnabled = true;
            this.listGrad.HorizontalScrollbar = true;
            this.listGrad.Location = new System.Drawing.Point(207, 24);
            this.listGrad.Name = "listGrad";
            this.listGrad.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listGrad.Size = new System.Drawing.Size(92, 329);
            this.listGrad.TabIndex = 7;
            // 
            // listAdr
            // 
            this.listAdr.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listAdr.FormattingEnabled = true;
            this.listAdr.HorizontalScrollbar = true;
            this.listAdr.Location = new System.Drawing.Point(109, 24);
            this.listAdr.Name = "listAdr";
            this.listAdr.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listAdr.Size = new System.Drawing.Size(92, 329);
            this.listAdr.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 5;
            this.label8.Text = "Naziv";
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.Location = new System.Drawing.Point(8, 359);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 23);
            this.btnPrikazi.TabIndex = 4;
            this.btnPrikazi.Text = "Prikazi";
            this.btnPrikazi.UseVisualStyleBackColor = true;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(403, 359);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(190, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Azuriraj nalaziste";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(11, 24);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(92, 329);
            this.listBox1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightGray;
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.btnIzbrisi);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.tbxIzbrisi);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(623, 399);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Izbrisi nalaziste";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(101, 354);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(152, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Izbrisi sva nalazista";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Location = new System.Drawing.Point(178, 68);
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(75, 23);
            this.btnIzbrisi.TabIndex = 2;
            this.btnIzbrisi.Text = "Izbrisi";
            this.btnIzbrisi.UseVisualStyleBackColor = true;
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Naziv:";
            // 
            // tbxIzbrisi
            // 
            this.tbxIzbrisi.Location = new System.Drawing.Point(70, 29);
            this.tbxIzbrisi.Name = "tbxIzbrisi";
            this.tbxIzbrisi.Size = new System.Drawing.Size(183, 20);
            this.tbxIzbrisi.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightGray;
            this.tabPage4.Controls.Add(this.tbxJMBG);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.cbxDodajNalazista);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.button3);
            this.tabPage4.Controls.Add(this.tbxIskopineA);
            this.tabPage4.Controls.Add(this.tbxFirmaA);
            this.tabPage4.Controls.Add(this.tbxImeA);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(623, 399);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Dodaj ahreologa";
            // 
            // cbxDodajNalazista
            // 
            this.cbxDodajNalazista.FormattingEnabled = true;
            this.cbxDodajNalazista.Location = new System.Drawing.Point(115, 124);
            this.cbxDodajNalazista.Name = "cbxDodajNalazista";
            this.cbxDodajNalazista.Size = new System.Drawing.Size(100, 21);
            this.cbxDodajNalazista.Sorted = true;
            this.cbxDodajNalazista.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Dodela nalazista:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(100, 171);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "Dodaj arheologa";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tbxIskopineA
            // 
            this.tbxIskopineA.Location = new System.Drawing.Point(115, 95);
            this.tbxIskopineA.Name = "tbxIskopineA";
            this.tbxIskopineA.Size = new System.Drawing.Size(100, 20);
            this.tbxIskopineA.TabIndex = 6;
            // 
            // tbxFirmaA
            // 
            this.tbxFirmaA.Location = new System.Drawing.Point(115, 69);
            this.tbxFirmaA.Name = "tbxFirmaA";
            this.tbxFirmaA.Size = new System.Drawing.Size(100, 20);
            this.tbxFirmaA.TabIndex = 5;
            // 
            // tbxImeA
            // 
            this.tbxImeA.Location = new System.Drawing.Point(115, 14);
            this.tbxImeA.Name = "tbxImeA";
            this.tbxImeA.Size = new System.Drawing.Size(100, 20);
            this.tbxImeA.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 98);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Iskopine i otkrica:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Firma:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ime i prezime:";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.LightGray;
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.lbNal);
            this.tabPage5.Controls.Add(this.lbFirma);
            this.tabPage5.Controls.Add(this.lbIme);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(623, 399);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Prikazi Arheologe";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(334, 349);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 44);
            this.button4.TabIndex = 6;
            this.button4.Text = "Prikazi";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lbNal
            // 
            this.lbNal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbNal.FormattingEnabled = true;
            this.lbNal.HorizontalScrollbar = true;
            this.lbNal.Location = new System.Drawing.Point(334, 33);
            this.lbNal.Name = "lbNal";
            this.lbNal.Size = new System.Drawing.Size(120, 290);
            this.lbNal.TabIndex = 5;
        
            // 
            // lbFirma
            // 
            this.lbFirma.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbFirma.FormattingEnabled = true;
            this.lbFirma.HorizontalScrollbar = true;
            this.lbFirma.Location = new System.Drawing.Point(171, 33);
            this.lbFirma.Name = "lbFirma";
            this.lbFirma.Size = new System.Drawing.Size(120, 290);
            this.lbFirma.TabIndex = 4;
            // 
            // lbIme
            // 
            this.lbIme.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbIme.FormattingEnabled = true;
            this.lbIme.HorizontalScrollbar = true;
            this.lbIme.Location = new System.Drawing.Point(11, 33);
            this.lbIme.Name = "lbIme";
            this.lbIme.Size = new System.Drawing.Size(120, 290);
            this.lbIme.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(331, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Nalaziste:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(168, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Firma:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ime i Prezime:";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.LightGray;
            this.tabPage6.Controls.Add(this.tbxJMBGIzbrisi);
            this.tabPage6.Controls.Add(this.label23);
            this.tabPage6.Controls.Add(this.button6);
            this.tabPage6.Controls.Add(this.tbxIzbrisiA);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Controls.Add(this.button5);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(623, 399);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Izbrisi arheologe";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(172, 109);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 3;
            this.button6.Text = "Izbrisi";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tbxIzbrisiA
            // 
            this.tbxIzbrisiA.Location = new System.Drawing.Point(101, 28);
            this.tbxIzbrisiA.Name = "tbxIzbrisiA";
            this.tbxIzbrisiA.Size = new System.Drawing.Size(146, 20);
            this.tbxIzbrisiA.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 31);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Ime i prezime:";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(56, 357);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(191, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "Izbrisi sve arheologe";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "JMBG:";
            // 
            // tbxJMBG
            // 
            this.tbxJMBG.Location = new System.Drawing.Point(115, 40);
            this.tbxJMBG.Name = "tbxJMBG";
            this.tbxJMBG.Size = new System.Drawing.Size(100, 20);
            this.tbxJMBG.TabIndex = 11;
            this.tbxJMBG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxJMBG_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "JMBG:";
            // 
            // tbxJMBGIzbrisi
            // 
            this.tbxJMBGIzbrisi.Location = new System.Drawing.Point(101, 70);
            this.tbxJMBGIzbrisi.Name = "tbxJMBGIzbrisi";
            this.tbxJMBGIzbrisi.Size = new System.Drawing.Size(146, 20);
            this.tbxJMBGIzbrisi.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(635, 428);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form2";
            this.Text = "Arheologija Srbije";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDodajNalaziste;
        private System.Windows.Forms.TextBox tbxNaziv;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tbxGradska;
        private System.Windows.Forms.TextBox tbxAdresa;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxOpstina;
        private System.Windows.Forms.TextBox tbxMesto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxZavod;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.Button btnIzbrisi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxIzbrisi;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listZavod;
        private System.Windows.Forms.ListBox listOps;
        private System.Windows.Forms.ListBox listMesto;
        private System.Windows.Forms.ListBox listGrad;
        private System.Windows.Forms.ListBox listAdr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbxDodajNalazista;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox tbxIskopineA;
        private System.Windows.Forms.TextBox tbxFirmaA;
        private System.Windows.Forms.TextBox tbxImeA;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListBox lbNal;
        private System.Windows.Forms.ListBox lbFirma;
        private System.Windows.Forms.ListBox lbIme;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox tbxIzbrisiA;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbxJMBG;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbxJMBGIzbrisi;
        private System.Windows.Forms.Label label23;
    }
}